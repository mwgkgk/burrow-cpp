ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUILD_DIR:=$(ROOT_DIR)/build
DEST_DIR:=${HOME}/bin

all: clean
	@echo -e "\033[0;37m"`date +'%a, %b %d %H:%M:%S'`" - compiling..\033[0m"
	g++ `fltk-config --cxxflags` src/main.cpp src/burrow.cpp src/burrowbox.cpp \
	src/burrowwindow.cpp \
	`fltk-config --ldflags` -o $(BUILD_DIR)/burrow -lboost_system \
		-lboost_filesystem -fno-rtti -std=c++2a -I .
	@echo -e "\033[0;37m"`date +'%a, %b %d %H:%M:%S'`" \033[0;32m- done\033[0m"

clean:
	@echo -e `date +'%a, %b %d %H:%M:%S'`"\033[0;37m - cleaning..\033[0m"
	mkdir -p "$(BUILD_DIR)/"
	rm -rf $(BUILD_DIR)/*

install: all
	@echo -e `date +'%a, %b %d %H:%M:%S'`"\033[0;37m - installing..\033[0m"
	cp "$(BUILD_DIR)/burrow" "$(DEST_DIR)/burrow"

t:
	g++ test/tests.cpp src/burrow.cpp -o $(BUILD_DIR)/test-runner -static \
		-lboost_system -lboost_filesystem \
		-lboost_unit_test_framework \
		-std=c++2a -I . \
		&& $(BUILD_DIR)/test-runner
