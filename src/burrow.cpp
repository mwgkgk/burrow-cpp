#include <algorithm>
#include "include/burrow.h"

bool Burrow::current_paths_are_correct() {
  int errors = 0;
  for (fs::path &p : current_paths_)
    errors += static_cast<int>(path_is_correct(p));
  return static_cast<bool>(errors);
}

bool Burrow::page_is_correct(const int &p) {
  // Simple check to speed up common case:
  if (p == 0)
    return true;
  int s = nodes_.size();  // If we calculate it on the spot results are weird.
  return ((s - (page_size_ - 1 + (p - 1) * (page_size_ - 2)) > 0)
      && (p >= 0));
}

Burrow::LoadStatus Burrow::ascend() {
  if (path_is_correct(current_paths_.front()) &&
      current_paths_.front().has_parent_path()) {
    fs::path new_path = current_paths_.front().parent_path();
    if (path_is_correct(new_path)) {
      current_paths_ = { new_path };
      if (load_nodes() > 0)
        return LoadStatus::SUCCESS;
      else
        return LoadStatus::SUCCESS_WITH_NO_ITEMS;
    } else {
      return LoadStatus::PATH_INCORRECT;
    }
  } else {
    return LoadStatus::PATH_INCORRECT;
  }
}

Burrow::LoadStatus Burrow::descend(const fs::path &node) {
  if (path_is_correct(node)) {
    current_paths_ = { node };
    if (load_nodes() > 0)
      return LoadStatus::SUCCESS;
    else
      return LoadStatus::SUCCESS_WITH_NO_ITEMS;
  } else {
    return LoadStatus::PATH_INCORRECT;
  }
}

int Burrow::adapt_page_size(const int &s) {
  for (int &i : page_sizes_) {
    if (s <= i)
      return i;
  }
  for (int j = 2; j <= 5; ++j)
    for (int &i : page_sizes_) {
       if (s <= j*i)
         return i;
    }
  // If it's greater than 5 * greatest size, just return greatest size.
  return page_sizes_.back();
}

int Burrow::adapt_row_size(const int &s) {
    // We want to choose biggest fitting size
    int row_size = row_sizes_.front();
    for (int size : row_sizes_)
      if (s % size == 0) { row_size = size; }
    return row_size;
}

int Burrow::load_nodes() {
  current_page_ = 0;
  nodes_ = {};  // use it for dirs at first to sort separately
  NodeVector files = {};

  for (fs::path &p : current_paths_)
    for (auto i = fs::directory_iterator(p);
         i != fs::directory_iterator(); ++i) {
      if (is_directory(i->path()))
        nodes_.push_back(i->path());
      else if (i->status().permissions() & fs::owner_exe)
        files.push_back(i->path());
    }

  std::sort(nodes_.begin(), nodes_.end());
  std::sort(files.begin(), files.end());
  nodes_.insert(nodes_.end(), files.begin(), files.end());

  int s = nodes_.size();

  if (var_page_size_) {
    page_size_ = adapt_page_size(s);
    row_size_ = adapt_row_size(page_size_);
    column_size_ = page_size_ / row_size_;
  }

  int remainder =  (s % page_size_ == 0) ? 0 : 1;
  pages_ = s / page_size_ + remainder;

  return s;  // Success. Number of nodes.
}

NodeVector Burrow::construct_page() {
  int s = nodes_.size();
  if (s <= page_size_ || disable_paging_)
    return nodes_;

  // First page:
  int one_btn_size = page_size_ - 1;
  if (current_page_ == 0)
    return NodeVector(nodes_.begin(), nodes_.begin() + one_btn_size);

  int two_btn_size = page_size_ - 2;
  int left_margin = one_btn_size + (current_page_ - 1) * two_btn_size;

  // Last page:
  if (s - left_margin <= one_btn_size)
    return NodeVector(nodes_.begin() + left_margin, nodes_.end());
  // Other pages:
  else
    return NodeVector(nodes_.begin() + left_margin, nodes_.begin() +
        left_margin + two_btn_size);
}
