#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/names.h>
#include <X11/Xlib.h>
#include <iostream>
#include <cctype>
#include <string>
#include <algorithm>
#include "include/burrowwindow.h"

BurrowWindow::BurrowWindow(int w, int h, Burrow* b,
    const char* title = 0) :
  Fl_Window(w, h, title) {
  xclass("burrow");
  color(FL_BLACK);

  burrow_ = b;
  populate();

  end();
  // set_modal();
  show();
}
int BurrowWindow::handle(int e) {
  int ret = Fl_Window::handle(e);
  int keysym = Fl::event_key();
  const char* et = Fl::event_text();
  std::string ev_str = event_code_to_str(keysym, Fl::event_state());
  if (fl_eventnames[e] == "FL_KEYDOWN") {
    // Highlight the matching box(es);
    for (BurrowBox* box : boxes_)
      if (box->shortcut_text() == ev_str)
        box->highlight_on();
    // Always-valid shortcuts for next/prev pages:
    if (ev_str == "shift-period") {
      for (BurrowBox* box : boxes_)
        if (box->button_text() == " > ")
          box->highlight_on();
    } else if (ev_str == "shift-comma") {
      for (BurrowBox* box : boxes_)
        if (box->button_text() == " < ")
          box->highlight_on();
    }
  } else if (fl_eventnames[e] == "FL_KEYUP") {
    // Click
    // Placing this first to speed up common case.
    for (BurrowBox* box : boxes_)
      if (box->shortcut_text() == ev_str) {
        box->click();
        return ret;
      }
    // Always-valid shortcuts for next/prev pages:
    if (ev_str == "shift-period") {
      for (BurrowBox* box : boxes_)
        if (box->button_text() == " > ") {
          box->click();
          return ret;
        }
    } else if (ev_str == "shift-comma") {
      for (BurrowBox* box : boxes_)
        if (box->button_text() == " < ") {
          box->click();
          return ret;
        }
    } else if (keysym == 96) {
      // 96 is the event_key() for ` (also known as backtick/XK_Grave/tilde).
      Burrow::LoadStatus status = burrow_->ascend();
      if (status == Burrow::LoadStatus::SUCCESS ||
          status == Burrow::LoadStatus::SUCCESS_WITH_NO_ITEMS) {
        repopulate();
      } else {
        std::cerr << "bad status" << "\n";
      }
    }
  }
  return ret;
}
void BurrowWindow::adapt_size(Burrow* b) {
  size(b->row_size() * (button_width_ + margin_) + margin_,
       b->column_size() * (button_height_ + margin_) + margin_);
}
void BurrowWindow::destroy_buttons() {
  for (BurrowBox* box : boxes_)
    delete box;
  boxes_.clear();
}
void BurrowWindow::populate(Burrow* b) {
  NodeVector nodes = b->get_current_page();

  if (nodes.size() <= 0)
    return;

  adapt_size(b);

  int row_size = b->row_size();  // 1-based;
  int column_size = b->column_size();
  int pages = b->pages();

  used_shortcuts_ = banned_shortcuts_;

  // Simplify the common case at the cost of code repetition:
  if (pages == 1) {
    for (int j = 0; j < column_size; ++j)
      for (int i = 0; i < row_size; ++i) {
        std::string filename = nodes.front().filename().string();
        std::string shortcut = extract_shortcut(filename);
        BurrowBox* box = new BurrowBox (
            i * (button_width_ + margin_) + margin_,
            j * (button_height_ + margin_) + margin_,
            button_width_, button_height_, shortcut,
            nodes.front().string(), filename);
        add(box);
        boxes_.push_back(box);
        nodes.erase(nodes.begin());
        if (nodes.size() == 0)
          goto done;
      }
  } else {  // We got < > buttons;
    int current_page = b->current_page();
    int column_size_0 = column_size - 1;  // Zero-based.
    int row_size_0 = row_size - 1;
    int pages_0 = pages - 1;
    for (int j = 0; j < column_size; ++j)
      for (int i = 0; i < row_size; ++i) {
        if (i == row_size_0 && j == column_size_0 && current_page < pages_0) {
          // Right button, >
          // For next/prev, always resort to pre-defined shortcuts:
          std::string shortcut = find_first_matching_shortcut("", shortcuts_);
          BurrowBox* box = new BurrowBox (
              i * (button_width_ + margin_) + margin_,
              j * (button_height_ + margin_) + margin_,
              button_width_, button_height_, shortcut,
              "Next page", " > ");
          add(box);
          boxes_.push_back(box);
        } else if (current_page > 0 && i == 0 && j == column_size_0) {
          // Left button, <
          // For next/prev, always resort to pre-defined shortcuts:
          std::string shortcut = find_first_matching_shortcut("", shortcuts_);
          BurrowBox* box = new BurrowBox (
              i * (button_width_ + margin_) + margin_,
              j * (button_height_ + margin_) + margin_,
              button_width_, button_height_, shortcut,
              "Previous page", " < ");
          add(box);
          boxes_.push_back(box);
        } else {
          if (nodes.size() > 0) {
            std::string filename = nodes.front().filename().string();
            std::string shortcut = extract_shortcut(filename);
            BurrowBox* box = new BurrowBox (
                i * (button_width_ + margin_) + margin_,
                j * (button_height_ + margin_) + margin_,
                button_width_, button_height_, shortcut,
                nodes.front().string(), filename);
            add(box);
            boxes_.push_back(box);
            nodes.erase(nodes.begin());
          }
        }
      }
  }

  done:
    return;
}
std::string BurrowWindow::find_first_matching_shortcut(
    const std::string &modifiers, const std::string &source) {
  if (source.length() > 0) {
    for (const char &c : source) {
      std::string c_lower(1, tolower(c));
      if (std::find(used_shortcuts_.begin(), used_shortcuts_.end(),
            modifiers + c_lower) == used_shortcuts_.end()) {
        used_shortcuts_.push_back(c_lower);
        return c_lower;
      }
    }
  }
  return "";
}
std::string BurrowWindow::extract_shortcut(std::string &source) {
  bool do_parse = true;
  std::string m_ctrl = "", m_alt = "", m_shift = "", m_meta = "";
  while (do_parse) {
    if (source.compare(0, 5, "ctrl-") == 0) {
      source.erase(0, 5);
      m_ctrl = "ctrl-";
    } else if (source.compare(0, 4, "alt-") == 0) {
      source.erase(0, 4);
      m_alt = "alt-";
    } else if (source.compare(0, 6, "shift-") == 0) {
      source.erase(0, 6);
      m_shift = "shift-";
    } else if (source.compare(0, 5, "meta-") == 0) {
      source.erase(0, 5);
      m_meta = "meta-";
    } else {
      do_parse = false;
    }
  }
  std::string modifiers = m_ctrl + m_alt + m_shift + m_meta;
  std::string shortcut = "";
  // If the name starts with a char and a dash, assume its a shortcut, e.g.
  // w-browsers; (we remove the shortcut from the original string)
  if (source.length() > 2 && source[1] == '-' &&
      std::find(used_shortcuts_.begin(), used_shortcuts_.end(), modifiers +
                 source.substr(0, 1)) == used_shortcuts_.end()) {
    shortcut.push_back(tolower(source[0]));
    used_shortcuts_.push_back(modifiers + source.substr(0, 1));
    source.erase(0, 2);
  }
  // Try to derive the shortcut from the filename:
  if (shortcut == "")
    shortcut = find_first_matching_shortcut(modifiers, source);
  // Resort to pre-defined shortcuts:
  if (shortcut == "")
    shortcut = find_first_matching_shortcut(modifiers, shortcuts_);
  return modifiers + shortcut;
}
std::string BurrowWindow::event_code_to_str(const int keysym,
    const int state) {
  std::string state_text = "";
  // The ordering is important and replicates ordering in extract_shortcut
  if (state & FL_CTRL)
    state_text.append("ctrl-");
  if (state & FL_ALT)
    state_text.append("alt-");
  if (state & FL_SHIFT)
    state_text.append("shift-");
  if (state & FL_META)
    state_text.append("meta-");
  if (keysym > 0)
    return state_text + std::string(XKeysymToString(keysym));
  else
    return "";
}
