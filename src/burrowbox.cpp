#include <FL/fl_draw.H>
#include <FL/names.h>
#include <spawn.h>
#include <sys/wait.h>
#include "include/burrowbox.h"
#include "include/burrowwindow.h"

void BurrowBox::click() {
  highlight_off();
  if (button_text_ == " > ") {
    BurrowWindow* win = static_cast<BurrowWindow*>(window());
    win->next_page();
  } else if (button_text_ == " < ") {
    BurrowWindow* win = static_cast<BurrowWindow*>(window());
    win->prev_page();
  } else {
    if (fs::is_directory(tooltip_text_)) {
      BurrowWindow* win = static_cast<BurrowWindow*>(window());
      win->descend(tooltip_text_);
    } else {
      char* cm = const_cast<char*>(tooltip_text_.c_str());
      pid_t pid;
      char* argv[] = {cm, reinterpret_cast<char *>(0)};
      int status;
      status = posix_spawn(&pid, cm, NULL, NULL, argv,
          environ);

      // if (status == 0) {
      //   printf("Child id: %i\n", pid);
      //   if (waitpid(pid, &status, 0) != -1) {
      //     printf("Child exited with status %i\n", status);
      //   } else {
      //     perror("waitpid");
      //   }
      // } else {
      //   printf("posix_spawn: %s\n", strerror(status));
      // }
      exit(0);
    }
  }
}

int BurrowBox::handle(int e) {
  int ret = Fl_Box::handle(e);
  if (fl_eventnames[e] == "FL_RELEASE") {
    click();
  } else if (fl_eventnames[e] == "FL_ENTER") {
    highlight_on();
  } else if (fl_eventnames[e] == "FL_LEAVE") {
    highlight_off();
  }
  return ret;
}

void BurrowBox::draw() {
  fl_font(FL_HELVETICA, 11);
  fl_rectf(x(), y(), w(), h(), FL_BLACK);
  fl_color(draw_color_);
  fl_draw_box(FL_BORDER_FRAME, x()+1, y()+1, w()-1, h()-1, draw_color_);
  tooltip(tooltip_text_.c_str());
  fl_color(shortcut_color_);
  float width = fl_width(shortcut_text_.c_str());
  fl_draw(shortcut_text_.c_str(),
      x() + w() - 7 - static_cast<int>(width),
      y() + h() - 8);
  if (fs::is_directory(tooltip_text_)) {
    fl_color(main_color_);
  } else {
    fl_color(shortcut_color_);
  }
  fl_draw(button_text_.c_str(), x()+8, y()+18);
}
