#include <FL/Fl.H>
#include <iostream>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include "include/burrow.h"
#include "include/burrowwindow.h"

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

int main(int argc, char** argv) {
  Burrow *b;

  signal(SIGSEGV, handler);

  if (argc == 2) {  // The most common use, 1 arg
    fs::path p = argv[1];
    b = new Burrow(p);
  } else if (argc > 2) {  // Multiple directories
    NodeVector arguments(argv + 1, argv + argc);
    b = new Burrow(arguments);
  } else {  // Default case, no args
    b = new Burrow();
  }

  // TODO: read from xrdb (.Xresources) or config.h
  // Somehow triggers much faster without this line.
  /* Fl::set_font(FL_HELVETICA, "-*-anka/coder narrow-medium-r-*-*-10-*-*-*-*-*-*-*"); */

  Burrow::LoadStatus status = b->init_status();

  if (status == Burrow::LoadStatus::SUCCESS ||
      status == Burrow::LoadStatus::SUCCESS_WITH_NO_ITEMS) {
    BurrowWindow* window = new BurrowWindow(340, 180, b, "burrow");

    return Fl::run();
  } else {
    std::cerr << "Incorrect path:" << "\n";
    for (fs::path p : b->current_paths())
      std::cerr << p << "\n";
  }
}
