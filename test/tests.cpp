// Many of the tests rely on data in test/test-root* (including links and file
// permissions) which might not be 100% replicated when cloning from repo.

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include "include/burrow.h"

BOOST_AUTO_TEST_CASE(test_creating_object_no_args_works) {
  Burrow *b = new Burrow();
  delete b;
}

BOOST_AUTO_TEST_CASE(test_creating_object_with_test_path_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  NodeVector test_paths = { test_dir };
  BOOST_CHECK(b->current_paths() == test_paths);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_creating_object_with_multiple_paths_works) {
  fs::path test_dir1(fs::current_path() /
      "test" / "test-root");
  fs::path test_dir2(fs::current_path() /
      "test" / "test-root" / "dir1");
  NodeVector test_paths = { test_dir1, test_dir2 };
  Burrow *b = new Burrow(test_paths);
  BOOST_CHECK(b->current_paths() == test_paths);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_test_root_is_correct) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  BOOST_CHECK(b->current_paths_are_correct() == true);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_init_status_success_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  BOOST_CHECK(b->init_status() == Burrow::LoadStatus::SUCCESS);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_init_status_success_empty_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root" / "dir1" / "dir1-1");
  Burrow *b = new Burrow(test_dir);
  BOOST_CHECK(b->init_status() == Burrow::LoadStatus::SUCCESS_WITH_NO_ITEMS);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_load_nodes_works) {
  // Also tests the ordering: by name, directories before files.
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  NodeVector nodes = b->nodes();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_non_executable_files_arent_included) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root" / "dir2");
  Burrow *b = new Burrow(test_dir);
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir2-1", "dir2-2", "file2-1"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_links_are_included) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root" / "dir3");
  Burrow *b = new Burrow(test_dir);
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir2", "dir3-1", "file3-1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_load_nodes_works_with_multiple_paths) {
  fs::path test_dir1(fs::current_path() /
      "test" / "test-root");
  fs::path test_dir2(fs::current_path() /
      "test" / "test-root-2");
  NodeVector test_paths = { test_dir1, test_dir2 };
  Burrow *b = new Burrow(test_paths);
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "dir4", "dir5", "file1",
    "file2", "file3"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_descend_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->descend(b->nodes().front());  // Descend dir1
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1-1", "dir1-2", "file1-1"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_descend_works_with_multiple_paths) {
  fs::path test_dir1(fs::current_path() /
      "test" / "test-root");
  fs::path test_dir2(fs::current_path() /
      "test" / "test-root-2");
  NodeVector test_paths = { test_dir1, test_dir2 };
  Burrow *b = new Burrow(test_paths);
  b->descend(b->nodes().front());  // Descend dir1
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1-1", "dir1-2", "file1-1"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_descend_does_not_work_with_incorrect_path) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  fs::path incorrect_dir(fs::current_path() /
      "test" / "test-root-incorrect");
  Burrow *b = new Burrow(test_dir);
  Burrow::LoadStatus res = b->descend(incorrect_dir);
  BOOST_CHECK(res == Burrow::LoadStatus::PATH_INCORRECT);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_ascend_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root" / "dir1");
  Burrow *b = new Burrow(test_dir);
  b->ascend();
  // First check path:
  fs::path test_dir1(fs::current_path() /
      "test" / "test-root");
  NodeVector test_paths = { test_dir1 };
  BOOST_CHECK(b->current_paths() == test_paths);
  // Then check nodes.
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_ascend_works_with_multiple_paths) {
  // Behavior is: ascend the first path on the list.
  fs::path test_dir(fs::current_path() /
      "test" / "test-root" / "dir1");
  fs::path test_dir2(fs::current_path() /
      "test" / "test-root-2");
  NodeVector test_paths = { test_dir, test_dir2 };
  Burrow *b = new Burrow(test_paths);
  b->ascend();
  // First check path:
  fs::path test_dir1(fs::current_path() /
      "test" / "test-root");
  test_paths = { test_dir1 };
  BOOST_CHECK(b->current_paths() == test_paths);
  // Then check nodes.
  NodeVector nodes = b->nodes();
  for (fs::path &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}
BOOST_AUTO_TEST_CASE(test_current_page_is_defaulted_to_0) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  BOOST_CHECK(b->current_page() == 0);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_get_current_page_works_with_just_one_page) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(10);
  NodeVector nodes = b->get_current_page();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_get_page_works_with_more_than_one_page) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(3);
  // Test first page.
  NodeVector nodes = b->get_current_page();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2" };
  BOOST_CHECK(nodes == test_nodes);
  // Test second page.
  nodes = b->get_page(1);
  for (auto &s : nodes)
    s = s.filename();
  test_nodes = {"dir3"};
  BOOST_CHECK(nodes == test_nodes);
  // Test last page.
  nodes = b->get_page(2);
  for (auto &s : nodes)
    s = s.filename();
  test_nodes = {"file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_get_page_returns_empty_when_incorrect) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(10);
  NodeVector nodes = b->get_page(2);
  NodeVector test_nodes = {};
  BOOST_CHECK(nodes == test_nodes);
  nodes = b->get_page(-1);
  test_nodes = {};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_turning_pages_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(3);
  // Test first page.
  NodeVector nodes = b->get_current_page();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2" };
  BOOST_CHECK(nodes == test_nodes);
  // Test next page.
  nodes = b->get_next_page();
  for (auto &s : nodes)
    s = s.filename();
  test_nodes = {"dir3"};
  BOOST_CHECK(nodes == test_nodes);
  // Test previous page.
  nodes = b->get_prev_page();
  for (auto &s : nodes)
    s = s.filename();
  test_nodes = {"dir1", "dir2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_last_page_works_when_smaller_than_pagesize) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(4);
  // Test first page.
  NodeVector nodes = b->get_current_page();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3" };
  BOOST_CHECK(nodes == test_nodes);
  // Test last page.
  nodes = b->get_next_page();
  for (auto &s : nodes)
    s = s.filename();
  test_nodes = {"file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_turning_pages_returns_empty_in_edge_cases) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(10);
  NodeVector nodes = b->get_next_page();
  NodeVector test_nodes = {};
  BOOST_CHECK(nodes == test_nodes);
  BOOST_CHECK(b->current_page() == 0);
  nodes = b->get_prev_page();
  test_nodes = {};
  BOOST_CHECK(nodes == test_nodes);
  BOOST_CHECK(b->current_page() == 0);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_cant_set_page_size_to_incorrect_values) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  int prev_page_size = b->page_size();
  b->set_page_size(-1);
  BOOST_CHECK(b->page_size() == prev_page_size);
  b->set_page_size(0);
  BOOST_CHECK(b->page_size() == prev_page_size);
  b->set_page_size(1);
  BOOST_CHECK(b->page_size() == prev_page_size);
  b->set_page_size(2);
  BOOST_CHECK(b->page_size() == prev_page_size);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_cant_set_current_page_to_incorrect_values) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  int prev_current_page = b->current_page();
  b->set_current_page(-1);
  BOOST_CHECK(b->current_page() == prev_current_page);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_current_page_resets_on_refresh) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(3);
  b->set_current_page(2);
  BOOST_CHECK(b->current_page() == 2);
  b->refresh();
  BOOST_CHECK(b->current_page() == 0);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_copy_constructor_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(3);
  b->set_current_page(1);
  NodeVector nodes = b->nodes();
  Burrow *b2(b);
  NodeVector test_paths = { test_dir };
  BOOST_CHECK(b2->current_paths() == test_paths);
  BOOST_CHECK(b2->page_size() == 3);
  BOOST_CHECK(b2->current_page() == 1);
  BOOST_CHECK(b2->nodes() == nodes);
  delete b, b2;
}

Burrow* move_constructor_func(Burrow* b) {
  return b;
}

BOOST_AUTO_TEST_CASE(test_move_constructor_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_page_size(3);
  b->set_current_page(1);
  NodeVector nodes = b->nodes();
  Burrow *b2 = move_constructor_func(b);
  NodeVector test_paths = { test_dir };
  BOOST_CHECK(b2->current_paths() == test_paths);
  BOOST_CHECK(b2->page_size() == 3);
  BOOST_CHECK(b2->current_page() == 1);
  BOOST_CHECK(b2->nodes() == nodes);
  delete b, b2;
}

BOOST_AUTO_TEST_CASE(test_constructing_from_nodes_works) {
  NodeVector test_nodes = {"node1", "node2", "node3", "node4", "node5"};
  Burrow *b = new Burrow(test_nodes, true);
  BOOST_CHECK(b->nodes() == test_nodes);
  b->refresh();
  BOOST_CHECK(b->nodes() == test_nodes);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_adapt_page_size_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_var_page_size(true);
  b->set_page_sizes({3, 4, 5, 6});
  // Should be 5:
  b->refresh();
  BOOST_CHECK(b->page_size() == 5);
  // Should be 3:
  b->set_page_sizes({3, 4});
  b->refresh();
  BOOST_CHECK(b->page_size() == 3);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_adapt_row_size_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_var_page_size(true);
  b->set_page_sizes({3, 4, 5, 6});
  b->set_row_sizes({3, 4});
  b->refresh();
  // Should be 5:
  BOOST_CHECK(b->page_size() == 5);
  // Should be 3:
  BOOST_CHECK(b->row_size() == 3);

  b->set_page_sizes({3, 4});
  b->set_row_sizes({4, 5});
  b->refresh();
  // Should be 3:
  BOOST_CHECK(b->page_size() == 3);
  // Should be 4:
  BOOST_CHECK(b->row_size() == 4);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_pages_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_var_page_size(true);
  b->set_page_sizes({3, 4, 5, 6});
  b->refresh();
  // Should be 5:
  BOOST_CHECK(b->page_size() == 5);
  // Should be 2:
  BOOST_CHECK(b->pages() == 1);

  b->set_page_sizes({2});
  b->refresh();
  // Should be 2:
  BOOST_CHECK(b->page_size() == 2);
  // Should be 3:
  BOOST_CHECK(b->pages() == 3);
  delete b;
}

BOOST_AUTO_TEST_CASE(test_disable_paging_works) {
  fs::path test_dir(fs::current_path() /
      "test" / "test-root");
  Burrow *b = new Burrow(test_dir);
  b->set_disable_paging(true);
  b->set_page_size(3);  // With paging on, it'd be 3 pages.
  NodeVector nodes = b->get_current_page();
  for (auto &s : nodes)
    s = s.filename();
  NodeVector test_nodes = {"dir1", "dir2", "dir3", "file1", "file2"};
  BOOST_CHECK(nodes == test_nodes);
  delete b;
}
