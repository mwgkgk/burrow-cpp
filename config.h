#ifndef CONFIG_H_
#define CONFIG_H_

/* Appearance */

Fl_Color sel_text_color;
Fl_Color sel_border_color;
Fl_Color sel_bg_color;

Fl_Color norm_text_color;
Fl_Color norm_border_color;
Fl_Color norm_bg_color;

Fl_Color window_bg_color;

#endif  // CONFIG_H_
