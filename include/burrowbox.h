#ifndef INCLUDE_BURROWBOX_H_
#define INCLUDE_BURROWBOX_H_

#include <FL/Fl_Box.H>
#include <vector>
#include <string>

class BurrowBox : public Fl_Box {
 public:
  BurrowBox(int x, int y, int w, int h) : Fl_Box(x, y, w, h) {}
  BurrowBox(int x, int y, int w, int h, std::string shortcut_text,
      std::string tooltip_text, std::string button_text) : Fl_Box(x, y, w, h),
      shortcut_text_(shortcut_text), tooltip_text_(tooltip_text),
      button_text_(button_text) {}
  int handle(int e);
  void click();
  void highlight_on() {
    draw_color_ = shortcut_color_;
    redraw();
  }
  void highlight_off() {
    draw_color_ = background_color_;
    redraw();
  }

  std::string shortcut_text() { return shortcut_text_; }
  std::string button_text() { return button_text_; }

 private:
  void draw();

  // Colors
  // Fl_Color main_color_ = fl_rgb_color(213, 135, 0);
  Fl_Color main_color_ = fl_rgb_color(255, 235, 104);
  Fl_Color shortcut_color_ = fl_rgb_color(168, 213, 0);
  Fl_Color background_color_ = fl_rgb_color(39, 39, 39);
  Fl_Color draw_color_ = background_color_;

  // Data fields
  std::string shortcut_text_ = "w";
  std::string tooltip_text_ = "Test tooltip";
  std::string button_text_ = "test text";
};

#endif  // INCLUDE_BURROWBOX_H_
