#ifndef INCLUDE_BURROWWINDOW_H_
#define INCLUDE_BURROWWINDOW_H_

#include <FL/Fl_Window.H>
#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "include/burrow.h"
#include "include/burrowbox.h"

class BurrowWindow : public Fl_Window {
 public:
  BurrowWindow(int w, int h, Burrow* b, const char* title);
  int handle(int e);

  void next_page() {
    burrow_->turn_page_forward();
    repopulate();
  }
  void prev_page() {
    burrow_->turn_page_backward();
    repopulate();
  }
  void descend(std::string strpath) {
    Burrow::LoadStatus status = burrow_->descend(strpath);
    if (status == Burrow::LoadStatus::SUCCESS ||
        status == Burrow::LoadStatus::SUCCESS_WITH_NO_ITEMS) {
      repopulate();
    } else {
      std::cerr << "bad status." << "\n";
    }
  }

 private:
  void adapt_size(Burrow* b);
  void populate(Burrow* b);
  void destroy_buttons();
  void repopulate(Burrow* b) {
    destroy_buttons();
    populate(b);
    hide();
    show();
  }

  void populate() {populate(burrow_);}
  void repopulate() {repopulate(burrow_);}

  std::string find_first_matching_shortcut(const std::string &modifiers,
      const std::string &source);
  std::string extract_shortcut(std::string &source);
  std::string event_code_to_str(const int keysym, const int state);

  Burrow* burrow_ = nullptr;
  std::vector<BurrowBox*> boxes_ = {};
  int margin_ = 3;
  int button_height_ = 40;
  int button_width_ = 100;
  std::string shortcuts_ = "asdfgqwertzxcvb12345yuiophjklnm67890";
  std::vector<std::string> banned_shortcuts_ = { "-", "." };
  std::vector<std::string> used_shortcuts_ = banned_shortcuts_;
};

#endif  // INCLUDE_BURROWWINDOW_H_
