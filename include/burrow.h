#ifndef INCLUDE_BURROW_H_
#define INCLUDE_BURROW_H_

#include <boost/filesystem.hpp>
#include <vector>

namespace fs = boost::filesystem;
typedef std::vector<fs::path> NodeVector;

class Burrow {
 public:
  enum class LoadStatus : char {
    SUCCESS = 0,
    PATH_INCORRECT = 1,
    SUCCESS_WITH_NO_ITEMS = 2,
    UNINITIALIZED = 3,
  };

  Burrow() { init_status_ = init(); }
  explicit Burrow(const fs::path &p) {
    current_paths_ = { fs::canonical(p) };
    init_status_ = init();
  }
  explicit Burrow(const NodeVector &p) {
    current_paths_.clear();
    for (fs::path item : p)
      current_paths_.push_back(fs::canonical(item));
    init_status_ = init();
  }
  explicit Burrow(const NodeVector &n, bool from_nodes) : nodes_(n),
    constructed_from_nodes_(from_nodes) {
    init_status_ = init();
  }

  LoadStatus refresh() { return init_status_ = init(); }

  bool path_is_correct(const fs::path &p) {
    return (exists(p) && is_directory(p));
  }
  bool current_paths_are_correct();

  // Few words on paging: Because "next page" and "previous page" buttons in the
  // gui take place of regular buttons (when there's too many buttons to show
  // without paging them), page_size_ number does not equal the number of
  // nodes we're returning. For the first and last pages, actual number of nodes
  // is page_size_ - 1, for pages inbetween it is page_size_ - 2.

  bool page_is_correct(const int &p);
  bool current_page_is_correct() { return page_is_correct(current_page_); }
  NodeVector get_page(const int &p) {
    if (page_is_correct(p)) {
      current_page_ = p;
      return construct_page();
    } else {
      return {};
    }
  }
  NodeVector get_current_page() {
    return construct_page();
  }
  NodeVector get_prev_page() {
    if (page_is_correct(current_page_ - 1)) {
      --current_page_;
      return construct_page();
    } else { return {};
    }
  }
  NodeVector get_next_page() {
    if (page_is_correct(current_page_ + 1)) {
      ++current_page_;
      return construct_page();
    } else {
      return {};
    }
  }

  void turn_page_forward() {
    if (page_is_correct(current_page_ + 1)) {
      ++current_page_;
    }
  }
  void turn_page_backward() {
    if (page_is_correct(current_page_ - 1)) {
      --current_page_;
    }
  }

  LoadStatus ascend();  // Go up the directory tree.
  LoadStatus descend(const fs::path &node);

  NodeVector current_paths() { return current_paths_; }
  void set_current_paths(const NodeVector &p) {
    current_paths_.clear();
    for (fs::path item : p)
      current_paths_.push_back(fs::canonical(item));
    init_status_ = init();
  }
  void set_current_path(const fs::path  &p) {
    current_paths_ = { fs::canonical(p) };
  }

  NodeVector nodes() { return nodes_; }

  bool disable_paging() { return disable_paging_; }
  void set_disable_paging(const bool &b) { disable_paging_ = b; }

  int current_page() { return current_page_; }
  void set_current_page(const int &p) {
    if (page_is_correct(p))
      current_page_ = p;
  }

  int page_size() { return page_size_; }
  void set_page_size(const int &n) {
    if (n > 2)
      page_size_ = n;
  }

  // Number of pages:
  int pages() { return pages_; }
  void set_pages(const int &p) { pages_ = p; }

  LoadStatus init_status() { return init_status_; }

  bool constructed_from_nodes() { return constructed_from_nodes_; }

  bool var_page_size() { return var_page_size_; }
  void set_var_page_size(const bool &s) { var_page_size_ = s; }

  std::vector<int> page_sizes() { return page_sizes_; }
  void set_page_sizes(const std::vector<int> &s) { page_sizes_ = s; }

  std::vector<int> row_sizes() { return row_sizes_; }
  void set_row_sizes(const   std::vector<int> &s) { row_sizes_ = s; }

  int row_size() { return row_size_; }
  void set_row_size(const int &s) { row_size_ = s; }

  int column_size() { return column_size_; }
  void set_column_size(const int &s) { column_size_ = s; }

 private:
  LoadStatus init() {
    if (constructed_from_nodes_) {
      if (nodes_.size() > 0) {
        return LoadStatus::SUCCESS;
      } else {
        return LoadStatus::SUCCESS_WITH_NO_ITEMS;
      }
    } else if (current_paths_are_correct()) {
      if (load_nodes() > 0)
        return LoadStatus::SUCCESS;
      else
        return LoadStatus::SUCCESS_WITH_NO_ITEMS;
    } else {
      return LoadStatus::PATH_INCORRECT;
    }
  }

  // Reads current_paths_ contents into nodes_. Returns number of nodes.
  int load_nodes();

  // This happens only if the flag var_page_size_ is set. We adapt the page
  // size in load_nodes(). It persists between pages of the same bunch.
  int adapt_page_size(const int &s);
  int adapt_row_size(const int &s);

  NodeVector construct_page();

  NodeVector current_paths_ = { fs::canonical(".") };
  NodeVector nodes_ = {};

  bool disable_paging_ = false;

  int current_page_ = 0;
  int page_size_ = 9;  // Should always be > 2, see paging comment.
  int pages_ = 1;

  LoadStatus init_status_ = LoadStatus::UNINITIALIZED;

  // A flag to indicate the burrow has been constructed from nodes_:
  bool constructed_from_nodes_ = false;

  // A flag to adapt page size to reduce number of pages.
  bool var_page_size_ = true;

  // A set of sizes to choose from. These have to be > 2
  std::vector<int> page_sizes_ = {3, 4, 6, 8, 9, 12, 16, 20, 25};

  // Should be a sub-set of page_sizes_ :
  std::vector<int> row_sizes_ = {3, 4, 5};

  int row_size_ = 3;
  int column_size_ = 3;
};

#endif  // INCLUDE_BURROW_H_
